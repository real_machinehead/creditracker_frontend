import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import AuthReducer from './AuthReducer';
import Loanstypesreducer from '../Containers/data/loantypes/reducers';
//import auth from './AuthReducer'

/*const rootReducer = combineReducers({
    auth,
    form:formReducer
})*/

export default combineReducers({
    auth:AuthReducer,
    loantypes:Loanstypesreducer
});

//export default rootReducer