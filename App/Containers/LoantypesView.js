import React,{Component} from 'react';
import {connect} from 'react-redux';
import { StyleSheet, View, Text,FlatList } from 'react-native';
import { List, ListItem,Avatar } from 'react-native-elements';
import {Card,CardSection,Spinner,Input,Button} from '../Components/Common';
import { LOGIN_URL, USER_URL,lOANTYPES_URL,LOANTYPES_URL_CREATE } from '../Config/URLs'
import {
    loantypesCodeChanged,
    descriptionChanged,
    fetchloantype,
    authenticationfail,
    loantypeslistSuccess
} from '../Actions';
import {
    NETWORK_FAIL,
} from '../Actions/Types'

class loantypesview extends Component{
    
    constructor(props){
        super(props);
        this.state={ hasError:false,refreshing:false }
    }
    componentDidMount(){
      
       this.getLoantypes();
    }

    componentDidCatch(error, info){
        this.setState({ hasError:true });
    }

    getLoantypes=()=>{
        const {response,access_token}=this.props;
        fetch(lOANTYPES_URL,{
                    method:'GET',
                    headers: {
                        'Content-Type' : 'application/json',
                        'Accept' :'application/json',
                        'Authorization' : 'Bearer '+access_token
                    }
        })
        .then(response => response.json())
        .then(json => {
            if(json.hasOwnProperty('error'))
                this.props.authenticationfail(json);
            else 
               
                this.props.loantypeslistSuccess(json);
            })
            .catch((errors) => {
                    console.log('ERROR',errors.message);
            })
 
    };

    _onRefresh(){
        this.getLoantypes();
    }
    render(){
      if(this.state.hasError){
          return console.log('error');
      }
        return(

            <List>

            <FlatList
                data={this.props.response}
                onRefresh={()=>this._onRefresh()}
                refreshing={this.state.refreshing}
                renderItem={({ item }) => (

                <ListItem
 
                  title={`${item.name}`}
                  avatar={<Avatar
                        small
                        rounded
                        title={item.code}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}
                  />}  
                />
                
              )}
              keyExtractor={(item,index) => item.name}
            />
          </List>
        );
    }
} 


const styles={
    errorTextStyle:{
        fontSize:20,
        alignSelf:'center',
        color:'red'
    }
}
const mapStateToProps=({auth})=>{
    const{response,access_token}=auth;
    return{response,access_token};
};

export default connect(mapStateToProps,{ 
    loantypesCodeChanged,descriptionChanged,fetchloantype,loantypeslistSuccess
})(loantypesview);