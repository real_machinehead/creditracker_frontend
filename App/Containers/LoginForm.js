import React,{Component} from 'react';
import {connect} from 'react-redux';
import { View, Text } from 'react-native';
import {Card,CardSection,Spinner,Input,Button,Customalert} from '../Components/Common';
import {
  emailChanged,
    passwordChanged,
    loginUser
} from '../Actions'

class loginForm extends Component{

  onEmailchange(text){
        this.props.emailChanged(text);
    }
    onPasswordchange(text){
        this.props.passwordChanged(text);
    }

  onButtonPress(){
    const {email='lyndell.dobluis@gmail.com', password='Jingfreeks99'}=this.props;
    this.props.loginUser({email,password});

  };
    
    renderError(){
       
        if(this.props.error){
            return(
                <View style={{backgroundColor:'white'}}>
                    <Text style={styles.errorTextStyle} >
                        {this.props.error}
                    </Text>
                </View>
            );
        }
    }
    onTestingBUtton(){
     
        //const {email, password}=this.props;
        const {email, password}=this.props;
        this.props.loginUser({email,password});
     }
    renderButton(){
        console.log(this.props);
        if (this.props.loading){
           //return <Spinners />
          return <Spinner size='large' />
        }
        return (
            <Button onPress={this.onTestingBUtton.bind(this)}>
                Login
            </Button>
        );
    }
    render(){
   
        return(
            <Card>
                <CardSection>
                    <Input
                        label="email"
                        placeholder="email@email.com"
                        onChangeText={this.onEmailchange.bind(this)}
                        //value={this.props.email}
                        value='lyndell.dobluis@gmail.com'
                    />
                </CardSection>

                <CardSection>
                    <Input
                        secureTextEntry
                        label="password"
                        placeholder="password"
                        onChangeText={this.onPasswordchange.bind(this)}
                        //value={this.props.password}
                        value='Jingfreeks99'
                    />                  
                </CardSection>

                {this.renderError(this)}
                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>    
        );
    }
}

const styles={
    errorTextStyle:{
        fontSize:20,
        alignSelf:'center',
        color:'red'
    }
}
const mapStateToProps=({auth})=>{
    const{email,password,error,loading}=auth;
    return{email,password,error,loading };
};

export default connect(mapStateToProps,{ 
    emailChanged,passwordChanged,loginUser 
})(loginForm);
