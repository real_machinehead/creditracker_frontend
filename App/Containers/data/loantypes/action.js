import { LOGIN_URL, USER_URL,lOANTYPES_URL,LOANTYPES_URL_CREATE } from '../Config/URLs'
import { OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRECT } from '../Config/Settings'
import {
    LOGIN_USER_SUCCESS,
    USER_REQUEST, USER_SUCCESS, USER_FAILURE,
    EMAIL_CHANGED,PASSWORD_CHANGED,LOGIN_USER_FAIL,LOGIN_USER,NETWORK_FAIL,
    LOANTYPE_LIST,LOANTYPECODE_CHANGED,LOANDESCRIPTION_CHANGED,LOANTYPES_FAIL,LOANTYPES_SUCCESS,
    AUTHENTICATION_FAIL
} from './Types'
import { Alert, Text, TouchableOpacity, StyleSheet } from 'react-native'
import {Actions} from 'react-native-router-flux';

export const emailChanged=(text)=>{
    return{
        type:EMAIL_CHANGED,
        //payload:text
        payload:'lyndell.dobluis@gmail.com'
    };
};

export const passwordChanged=(text)=>{
    return{
        type:PASSWORD_CHANGED,
        //payload:text 
        payload:'Jingfreeks99'
    }
}

export const loantypesCodeChanged=(text)=>{
    return{
        type:LOANTYPECODE_CHANGED,
        payload:text 
    }
}

export const loandescriptionChanged=(text)=>{
    return{
        type:LOANDESCRIPTION_CHANGED,
        payload:text 
    }
}

export const loantypeslistSuccess=(json)=>{
    return{
        type:LOANTYPE_LIST,
        payload:json
    
    };
    //dispatch({type:LOANTYPE_LIST,payload:json});
};

export const postLoantypes=({loantypecode,loantypedesc,access_token})=>{
    return (dispatch) => {
        //console.log('jsons')
        loginUserRequest(dispatch);
        return fetch(LOANTYPES_URL_CREATE,{
            method:'POST',
            headers: {
                'Content-Type' : 'application/json',
                'Accept' :'application/json',
                'Authorization' : 'Bearer '+access_token
            },
            body: JSON.stringify({
                client_id: OAUTH_CLIENT_ID,
                client_secret: OAUTH_CLIENT_SECRECT,
                code:loantypecode,
                name: loantypedesc
            })
        })
        .then(response => response.json())
        .then(json => {
            console.log('json',loantypecode,json)
            if(json.hasOwnProperty('error'))
                //dispatch(loginFailure(email,json.message))
                //loginUserFail(dispatch);
                console.log('login success');
                //Alert.alert(json.message)
            else 
                //dispatch({type:'LOGIN_USER_SUCCESS',payload:json})
                //dispatch(loginSuccess(email,json))
               
                if(json.success===false){
                    dispatch({ type:LOANTYPES_FAIL});
                    console.log('Failed to insert Loantypes transaction, Please check your data');
                }else{
                    //console.log('login failed',json.success);
                    dispatch({type:LOANTYPES_SUCCESS})
                    //loantypesvalidationfail(dispatch);     
                }
       
                //loginSuccess(dispatch,email,json);
        })
        .catch((errors) => {
            console.log('ERRORs',errors.message);
            dispatch({type:NETWORK_FAIL,payload:errors.message}); 
        })
    };    
};

const authenticationfail =(dispatch)=>{
    dispatch({ type:AUTHENTICATION_FAIL,payload:errors.message});
};

const loantypesvalidationfail=(dispatch)=>{
    dispatch({ type:LOANTYPES_FAIL});
}
export const loginUser=({email, password})=>{
    return (dispatch) => {
        console.log('jsons',email,password)
        //dispatch(loginRequest(email,password))
        //dispatch({type:LOGIN_USER}); 
        loginUserRequest(dispatch);
        return fetch(LOGIN_URL,{
            method:'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                grant_type:'password',
                client_id: OAUTH_CLIENT_ID,
                client_secret: OAUTH_CLIENT_SECRECT,
                username:email,
                password:password,
                scope:''
            })
        })
        .then(response => response.json())
        .then(json => {
            console.log('json',email,json)
            if(json.hasOwnProperty('error'))
                //dispatch(loginFailure(email,json.message))
                loginUserFail(dispatch);
                //Alert.alert(json.message)
            else 
                //dispatch({type:'LOGIN_USER_SUCCESS',payload:json})
                //dispatch(loginSuccess(email,json))
                loginSuccess(dispatch,email,json);
        })
        .catch((errors) => {
            console.log('ERROR',errors.message);
            dispatch({type:NETWORK_FAIL,payload:errors.message}); 
        })
    };
};


const loginUserFail =(dispatch)=>{
    dispatch({ type:LOGIN_USER_FAIL});
};

const loginUserRequest = (dispatch)=>{
    //console.log('test');
    dispatch({type:LOGIN_USER}); 
};

const postLoantypesRequest=(dispatch,response)=>{
    diapatch({type:POST_LOANTYPES_REQUEST,response:response})
};

const loginSuccess = (dispatch,email,response)=>{
    dispatch({
        type: LOGIN_USER_SUCCESS,
        payload: { email, response }
    });
    Actions.main();
};

/*export function loginSuccess(email, response) {
    return {
        type: LOGIN_USER_SUCCESS,
        payload: { email, response }
    }
    Actions.Menu();
}*/