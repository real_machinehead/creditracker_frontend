
export const EMAIL_CHANGED='email_changed';
export const PASSWORD_CHANGED='password_changed';

export const LOANTYPECODE_CHANGED='loantypecode_changed';
export const LOANDESCRIPTION_CHANGED='loandescription_changed';
export const LOANTYPE_LIST='loantype_list';
export const LOANTYPES_FAIL='loantypes_fail';
export const LOANTYPES_SUCCESS='loantypes_success';
export const CLEAR_FIELDS='clear_fields';

export const AUTHENTICATION_FAIL='authentication_fail';
export const POST_LOANTYPES_REQUEST='post_loantypes_request';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER_SUCCESS='login_user_success';
export const LOGIN_USER='login_user';
export const NETWORK_FAIL='network_fail';


