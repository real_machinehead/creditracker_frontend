import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    LOANTYPECODE_CHANGED,
    LOANDESCRIPTION_CHANGED,
    LOGIN_USER_FAIL,
    LOGIN_USER_SUCCESS,
    LOGIN_USER,NETWORK_FAIL,
    LOANTYPES_FAIL,
    LOANTYPES_SUCCESS,
    LOANTYPE_LIST,
    POST_LOANTYPES_REQUEST,
    AUTHENTICATION_FAIL
} from '../Actions/Types';

const INITIAL_STATE={
    email:'lyndell.dobluis@gmail.com',
    password:'Jingfreeks99',
    loantypecode:'',
    loantypedesc:'',
    user:null,
    error:'',
    success:false,
    loading:false,
    access_token:'',
    response:[]
};

export default (state=INITIAL_STATE,action)=>{
    console.log('actionlogs',action);
    switch (action.type){
        case EMAIL_CHANGED:
            return {...state,email:action.payload };
        case PASSWORD_CHANGED:
            return {...state,password:action.payload};
        case LOANTYPECODE_CHANGED:
            return {...state,loantypecode:action.payload};
        case LOANDESCRIPTION_CHANGED:
            return {...state,loantypedesc:action.payload};
        case POST_LOANTYPES_REQUEST:
            return {...state,response:action.payload.response.access_token};
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                loading:false,
                access_token:action.payload.response.access_token,error:''};
        case LOGIN_USER_FAIL:        
            return {...state,error:'Authentication Failed.',password:'',loading:false};
        case LOGIN_USER:
            //
            return {...state,loading:true,error:''};
        case NETWORK_FAIL:
            return {...state,error:action.payload,loading:false,response:[]};
        case LOANTYPE_LIST:
            return {...state,response:action.payload}
        case LOANTYPES_FAIL:
            return {...state,error:'Failed to insert Loantypes transaction, Please check your data',
                loading:false,success:false
            }
        case LOANTYPES_SUCCESS:
            return {...state,error:'',loading:false,loantypecode:'',loantypedesc:'',success:true}
        case AUTHENTICATION_FAIL:
            return {...state,error:action.payload}
        default:         
            return state;
    }
};

