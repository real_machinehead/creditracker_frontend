import React,{Component} from 'react';
import {connect} from 'react-redux';
import { StyleSheet, View, Text,Modal } from 'react-native'
import {Card,CardSection,Spinner,Input,Button,Customalert} from '../../Components/Common';
import {
    loantypesCodeChanged,
    loandescriptionChanged,
    postLoantypes
} from '../../Containers/data/loantypes/action';


class loantypes extends Component{

    constructor(props){
        super(props);
        this.state={ hasError:false, modalVisible: false,success:false }
    }

    componentDidCatch(error, info){
        this.setState({ hasError:true });
    }

    onLoantypesCode(text){
        this.props.loantypesCodeChanged(text)
    }
    onDescription(text){
        this.props.loandescriptionChanged(text);
    }
    onPostloantypes(){
        const {loantypecode,loantypedesc,access_token,success}=this.props;
        this.props.postLoantypes({loantypecode,loantypedesc,access_token,success});
        this.setState({
            success:true,
            modalVisible:true
        })
    }
    renderError(){
        if(this.props.error){
            return(
                <View style={{backgroundColor:'white'}}>
                    <Text style={styles.errorTextStyle} >
                        {this.props.error}
                    </Text>
                </View>
            );
        }
    }
    renderButton(){
        console.log(this.props);
        if (this.props.loading){
           //return <Spinners />
          return <Spinner size='large' />
        }
        return (
            <Button onPress={this.onPostloantypes.bind(this)}>
                Save
            </Button>  
        );       
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible,success:visible});
    }

    onAlerBox(){
        if(this.state.success){
            //alert('error');    
            return(
                <Customalert title="ERROR" 
                    mainText="text1"
                    onTransparent={true}
                    onVisible={this.state.modalVisible}    
                    mainButtonOnPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                      }}
                />
                    );
        }
    }
    render(){
        console.log('Logss',this.props.success);
        if(this.state.hasError){
            return console.log('error');
        }
        return(
            <Card>
                <CardSection>
                    <Input
                        label="code"
                        placeholder="code"
                        onChangeText={this.onLoantypesCode.bind(this)}
                        value={this.props.loantypecode}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        label="description"
                        placeholder="description"
                        onChangeText={this.onDescription.bind(this)}
                        value={this.props.loantypedesc}
                    />
                </CardSection>
                {this.renderError(this)}
                <CardSection>
                    {this.renderButton()}
                </CardSection>
                {this.onAlerBox(this)}
            </Card>

        );
    }
} 


const styles={
    errorTextStyle:{
        fontSize:20,
        alignSelf:'center',
        color:'red'
    }
}
const mapStateToProps=({auth})=>{
    const{loantypecode,loantypedesc,error,loading,access_token,success}=auth;
    return{loantypecode,loantypedesc,error,loading,access_token,success };
};


export default connect(mapStateToProps,{ 
    loantypesCodeChanged,loandescriptionChanged,postLoantypes
})(loantypes);