import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import LoginForm from './Containers/LoginForm'

export default class Root extends Component {
    render(){
        return(
            <View>
                <LoginForm />
            </View>
            )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DDDDDD',
        alignItems:'center',
        justifyContent:'center'
    }
})