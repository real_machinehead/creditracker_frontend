import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight, View} from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

const Customalert=({title,mainTitle,buttonTitle,visible,mainButtonOnPress,onVisible,onTransparent})=>{
    console.log('log',title,mainTitle,buttonTitle,mainButtonOnPress);
    switch (title){
        case "ERROR":
            
            return(
                <View>
                        <Modal
                            animationType="slide"
                            transparent={onTransparent}
                            visible={onVisible}
                            onRequestClose={() => {
                                    alert('Modal has been closed.');
                            }}
                        >
                        <View style={{flex:1,flexDirection: 'column',justifyContent: 'center',alignItems: 'center'}}>
                            <View style={{backgroundColor: 'red',width:250,height:150,borderRadius:10}}>
                                <View style={{flex:1,flexDirection: 'row'}}>
                                    <View style={{flex:5,justifyContent: 'center',alignItems: 'center',marginBottom:5}}>
                                        <Text style={{fontSize:18,fontFamily:'helvetica',color:'white'}}>Lorem ipsum dolor sit</Text>
                                    </View>
                                    <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}}>
                                        <TouchableHighlight style={{width:50,height:50,justifyContent: 'center',alignItems: 'center'}}
                                        onPress={mainButtonOnPress}>
                                            <Icon name='close' size={25} color='white'/>
                                        </TouchableHighlight>
                                    </View>

                                </View>
                                <View style={{flex:4,flexDirection: 'column'}}>
                                    <View style={{flex:4,backgroundColor: '#e5e5e5',justifyContent: 'center',alignItems: 'center',borderBottomRightRadius:10,borderBottomLeftRadius:10}}>
                                      <View style={{marginTop:10,justifyContent: 'center',alignItems: 'center'}}>    
                                        <Text style={{fontSize:18,fontFamily:'helvetica'}}>Lorem ipsum dolor sit amet</Text>
                                      </View>
                                      <View style={{marginTop:15}}>
                                        <TouchableHighlight style={{width:150,height:50,justifyContent: 'center',alignItems: 'center',backgroundColor:'#00bfff',borderRadius:10}}
                                            onPress={mainButtonOnPress}>
                                             <Text style={{fontSize:18,fontWeight: 'bold',fontFamily:'helvetica'}}>OK</Text>
                                         </TouchableHighlight>
                                      </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        </Modal>

                </View>
            );
        case "SUCCESS":
            console.log('log',title);
            return(
                <View>
                        <Modal
                            animationType="slide"
                            transparent={onTransparent}
                            visible={onVisible}
                            onRequestClose={() => {
                                    alert('Modal has been closed.');
                            }}
                        >
                        <View style={{flex:1,flexDirection: 'column',justifyContent: 'center',alignItems: 'center'}}>
                            <View style={{backgroundColor: 'lime',width:250,height:150,borderRadius:10}}>
                                <View style={{flex:1,flexDirection: 'row'}}>
                                    <View style={{flex:5,justifyContent: 'center',alignItems: 'center',marginBottom:5}}>
                                        <Text style={{fontSize:18,fontFamily:'helvetica',color:'white'}}>Lorem ipsum dolor sit</Text>
                                    </View>
                                    <View style={{flex:1,justifyContent: 'center',alignItems: 'center'}}>
                                        <TouchableHighlight style={{width:50,height:50,justifyContent: 'center',alignItems: 'center'}}
                                        onPress={mainButtonOnPress}>
                                            <Icon name='close' size={25} color='white'/>
                                        </TouchableHighlight>
                                    </View>

                                </View>
                                <View style={{flex:4,flexDirection: 'column'}}>
                                    <View style={{flex:4,backgroundColor: '#e5e5e5',justifyContent: 'center',alignItems: 'center',borderBottomRightRadius:10,borderBottomLeftRadius:10}}>
                                      <View style={{marginTop:10,justifyContent: 'center',alignItems: 'center'}}>    
                                        <Text style={{fontSize:18,fontFamily:'helvetica'}}>Lorem ipsum dolor sit amet</Text>
                                      </View>
                                      <View style={{marginTop:15}}>
                                        <TouchableHighlight style={{width:150,height:50,justifyContent: 'center',alignItems: 'center',backgroundColor:'#00bfff',borderRadius:10}}
                                            onPress={mainButtonOnPress}>
                                             <Text style={{fontSize:18,fontWeight: 'bold',fontFamily:'helvetica'}}>OK</Text>
                                         </TouchableHighlight>
                                      </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        </Modal>

                </View>
            );       
        case CONFIM:
            return(
                <View>

                </View>
            );
    }
};
  export { Customalert };