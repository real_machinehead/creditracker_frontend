import React from 'react';
import { View,Text } from 'react-native';

const Listview = (props) => (
  <View style={styles.container}>
    <Text style={styles.text}>
      {props.children}
    </Text>
  </View>
);

const styles={
  containerStyle:{
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textStyle:{
    marginLeft: 12,
    fontSize: 16,
  }
};
export {Listview};