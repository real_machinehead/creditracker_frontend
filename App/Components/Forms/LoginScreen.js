import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form';
import { StyleSheet, View, Text, TextInput, Button,Alert } from 'react-native'
import {Card,CardSection,Spinner} from '../Common';
import TInput from './TInput'

class LoginScreen extends Component {
    constructor(props){
        super(props)
    }

    renderButton(){

        if(this.props.loading){
            return <Spinner size="large"/>;
        }
        return(

            <Button
                title = "Login"
                color = "#236CF5"
                style = {{backgroundColor:'#F8F8F8'}}
                onPress = {this.props.handleSubmit(this.props.onLogin)}
            />
        );
    }
    showLogin(props){
        let { onLogin, onLogout, onUser, handleSubmit, auth,loading } = props

        if(auth.access_token === '') {
            
            return (
           
             <Card>   
               <View style={styles.container}>   
                <CardSection>               
                    <Field 
                        style={styles.input} 
                        autoCapitalize="none" 
                        placeholder="Email" 
                        component={TInput} 
                        name={'email'} 
                    />
                </CardSection>    
                <CardSection>
                    <Field 
                        style={styles.input} 
                        autoCapitalize="none" 
                        placeholder="Password" 
                        secureTextEntry={true} 
                        component={TInput} 
                        name={'password'} />
                
                </CardSection>     
                <CardSection>
                    {this.renderButton(this.props)}
                </CardSection>
                </View>
            </Card>

            )

        }
        else {
            return (
                <View style={styles.container}>
                    <Card>
                        <CardSection>
                            <Text>{auth.email}</Text>
                        </CardSection>
                        <CardSection>
                            <Button title="My Info" color= "#236CF5" onPress={()=>onUser(auth)}/>
                        </CardSection>
                        <CardSection>
                            <Text style={{fontWeight:'bold'}}>{auth.name}</Text>
                        </CardSection>
                        <CardSection>
                            <Button title="Logout" color= "#236CF5" onPress={()=>onLogout()}/>
                        </CardSection>
                    </Card>
                </View>
                )
        }

    }
    render(){
        return this.showLogin(this.props)
        
   }
}

export default reduxForm({ form: 'login' })(LoginScreen); 

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white',
        width:400
    },
    input:{
        height:40,
        width:300,
        padding:5,
        borderWidth:0,
        borderBottomWidth:2,
        borderBottomColor:'#236CF5',
        borderColor:'gray',
        margin:10
    },
    headerContentStyle:{
        flexDirection:'column',
        justifyContent:'space-around'
    },
    headerTextStyle:{
        fontSize:18
    },
})