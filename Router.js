import React from 'react';
import {Scene, Router,Actions} from 'react-native-router-flux';
import loginForm from './App/Containers/Loginform';
import menu from './App/Containers/Menu';
import loantypes from './App/Containers/Loantypes';
import loantypesview from './App/Containers/LoantypesView';

const RouterComponent =()=>{
  return(
    <Router sceneStyle={{paddingTop:65}}>
      <Scene key="auth">
        <Scene key="login" component={loginForm} title="Please Login" initial/>
      </Scene>
      <Scene key="main">
        <Scene 
          onRight={()=>Actions.loantypes()}
          rightTitle="Add"
          key="menu" component={menu} title="Menu List" 
        />
      </Scene>
      <Scene key="loantypes" component={loantypes} title="Loan Types Creation">
      </Scene>

      <Scene key="loantypesview"  
        onRight={()=>Actions.loantypes()} rightTitle="Add"
        component={loantypesview} title="List of Loan Types">

      </Scene>
    </Router>
  );
};


export default RouterComponent;